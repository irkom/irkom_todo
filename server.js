var express = require('express');
var controller = require('./controller/todoController');

var app = express();

//set up template engine
app.set('view engine', 'ejs');


//static files
app.use(express.static('./public'));

//fire controller
controller(app);

//listen to port
app.listen(3000);
console.log('Your are listening port 3000');
